import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
// import { StudentsViewComponent } from 'src/app/students/view/students.view.component';
// import { StudentsAddComponent } from 'src/app/students/add/students.add.component';
// import { StudentsComponent } from 'src/app/students/list/students.component';
import { FileNotFoundComponent } from 'src/app/shared/file-not-found/file-not-found.component';

const appRoutes: Routes = [
  // { path: 'view', component: StudentsViewComponent },
  // { path: 'add', component: StudentsAddComponent },
  // { path: 'list', component: StudentsComponent },
  { path:'', redirectTo: '/list', pathMatch: 'full' },
  { path: '**', component: FileNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ] 
})
export class AppRoutingModule { }
